pragma solidity ^0.4.23;

import "./Ownable.sol";
import "./SafeMath.sol";

contract BlockWater is Ownable {

    struct EvidenceHistory {
        address buyer;
        uint256 oldMoisture;
        uint256 newMoisture;
    }

    EvidenceHistory[] History ;
    
    uint256 public WaterPrice;
    mapping (address=>uint256) Buyers;

    event BuyerRequestWatering(address buyer, uint256 second);
    event NewEvidenceUpdated();
    event BuyWater();

    constructor (uint256 _watePrice) public {
        owner = msg.sender;
        WaterPrice = _watePrice;
    }


    function buyWater() public payable {
        require(msg.value > 0);
        Buyers[msg.sender] += SafeMath.div(msg.value, WaterPrice);        
        emit BuyWater();
    }

    function getWaterBalance() public view returns (uint256) {
        return Buyers[msg.sender];
    }

    
    function requestWatering(uint256 _seconds) public returns (uint256 err) {        
    
        if (Buyers[msg.sender] >= _seconds) {
            
            Buyers[msg.sender] -= _seconds;
            emit BuyerRequestWatering(msg.sender, _seconds);
            err = 0;

        } else {
            err = 1;
        }

        return err;
    }

    function wateringCompleted(address _buyer, uint256 _oldMoisture, uint256 _newMoisture) public onlyOwner {
        History.push(EvidenceHistory({
            buyer: _buyer,
            oldMoisture: _oldMoisture,
            newMoisture: _newMoisture
        }));
        emit NewEvidenceUpdated();
    }

    function getHistoryLenght() public view returns (uint256){
        return History.length;
    }

    function getHistroy(uint256 index) public view returns (address buyer, uint256 oldMoisture, uint256 newMoisture) {
        return(History[index].buyer, History[index].oldMoisture, History[index].newMoisture);
    }

    function updateWaterPrice(uint256 _waterPrice) public onlyOwner {
        WaterPrice = _waterPrice;
    }

    function getContractBalance() public view onlyOwner returns (uint256) {
        return  address(this).balance;
    }
}